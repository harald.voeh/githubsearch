import React, { useState, useContext } from "react";

import GithubContext from "../../context/github/githubContext";
import AlertContext from "../../context/alert/alertContext";

const Search = () => {
  const githubContext = useContext(GithubContext);
  const alertContext = useContext(AlertContext);

  const [text, setText] = useState("");
  const handleChange = (e) => {
    setText(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (text === "") {
      alertContext.handleAlert("Please enter something", "light");
    } else {
      githubContext.handleSearch(text);
      setText("");
    }
  };

  return (
    <div className="">
      <form onSubmit={handleSubmit} className="form">
        <input
          value={text}
          onChange={handleChange}
          type="text"
          name="text"
          placeholder="Search Users..."
        />
        <input
          type="submit"
          value="search"
          className="btn btn-dark btn-block"
        />
      </form>
      {githubContext.users.length > 0 && (
        <button
          className="btn btn-light btn-block"
          onClick={githubContext.handleClear}
        >
          Clear
        </button>
      )}
    </div>
  );
};

export default Search;
