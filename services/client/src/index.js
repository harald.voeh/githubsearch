import React from "react";
import ReactDOM from "react-dom";

import { BrowserRouter } from "react-router-dom";
import GithubState from "./context/github/GithubState";
import AlertState from "./context/alert/AlertState";
import App from "./App";

import "font-awesome/css/font-awesome.css";
import Alert from "./components/layout/Alert";

ReactDOM.render(
  <GithubState>
    <AlertState>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </AlertState>
  </GithubState>,
  document.getElementById("root")
);
