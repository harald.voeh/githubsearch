import React, { useReducer } from "react";
import axios from "axios";
import GithubContext from "./githubContext";
import GithubReducer from "./githubReducer";
import {
  SEARCH_USERS,
  SET_LOADING,
  CLEAR_USERS,
  GET_USER,
  GET_REPOS,
} from "../types";

const GithubState = (props) => {
  const initialState = {
    users: [],
    user: {},
    repos: [],
    loading: false,
  };
  const [state, dispatch] = useReducer(GithubReducer, initialState);
  // Search Users
  const handleSearch = async (text) => {
    setLoading();
    const { data: users } = await axios.get(
      `https://api.github.com/search/users?q=${text}`
    );
    dispatch({
      type: SEARCH_USERS,
      payload: users.items,
    });
  };
  // Get User
  const getUser = async (username) => {
    setLoading();
    const { data: user } = await axios.get(
      `https://api.github.com/users/${username}`
    );
    dispatch({
      type: GET_USER,
      payload: user,
    });
  };
  // Get Repos
  const getRepos = async (username) => {
    setLoading();
    const { data: repos } = await axios.get(
      `https://api.github.com/users/${username}/repos?per_page=5&sort=created:asc`
    );
    dispatch({
      type: GET_REPOS,
      payload: repos,
    });
  };
  // Clear Users
  const handleClear = () => dispatch({ type: CLEAR_USERS });
  // Set Loading
  const setLoading = () => dispatch({ type: SET_LOADING });

  return (
    <GithubContext.Provider
      value={{
        users: state.users,
        user: state.user,
        repos: state.repos,
        loading: state.loading,
        handleSearch,
        handleClear,
        getUser,
        getRepos,
      }}
    >
      {props.children}
    </GithubContext.Provider>
  );
};

export default GithubState;
